﻿namespace IncidentsQueue
{
    public enum IncidentStates
    {
        New,                //Только что создан
        InitInDb,           //Создание записи в БД
        ReadyToProcessing,  //Готов к обработке диспетчером
        LongProcessWaiting, //Долгое ожидание обработки
        Processing,         //Обработка диспетчером
        ProcessingFinished  //Обработка завершена
    }
}