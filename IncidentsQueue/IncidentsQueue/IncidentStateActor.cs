﻿using System;
using System.Threading.Tasks;
using Akka.Actor;
using IncidentsQueue.Database;
using IncidentsQueue.Messages;

namespace IncidentsQueue
{
    public class IncidentStateActor : FSM<IncidentStates, IncidentStateDatа>
    {
        private readonly IActorRef _controller;

        private readonly IIncidentsRepository _repository;

        public IncidentStateActor(IActorRef controller, 
                                  IIncidentsRepository repository, 
                                  IncidentBase incident)
        {
            _controller = controller;
            _repository = repository;

            IncidentStateDatа stateDatа = new IncidentStateDatа()
            {
                Incident = incident
            };

            StartWith(IncidentStates.New, stateDatа);
            When(IncidentStates.New, LogicNew);
            When(IncidentStates.InitInDb, LogicInitInDb);
            When(IncidentStates.ReadyToProcessing, LogicReadyToProcessing, TimeSpan.FromSeconds(5));
            When(IncidentStates.LongProcessWaiting, LogicLongProcessWaiting);
            When(IncidentStates.Processing, LogicProcessing);
            When(IncidentStates.ProcessingFinished, LogicProcessingFinished);
            Initialize();
            Self.Tell(new SaveInDataBaseRequest());
        }

        private State<IncidentStates, IncidentStateDatа> LogicNew(Event<IncidentStateDatа> fsmEvent)
        {
            if (fsmEvent.FsmEvent is SaveInDataBaseRequest)
            {
                Task.Run(() => _repository.AddIncident(StateData.Incident))
                    .ContinueWith(t => new SaveInDataBaseInfoMsg() {Id = t.Result})
                    .PipeTo(Self);
                return GoTo(IncidentStates.InitInDb);
            }
            return null;
        }

        private State<IncidentStates, IncidentStateDatа> LogicInitInDb(Event<IncidentStateDatа> fsmEvent)
        {
            if (fsmEvent.FsmEvent is SaveInDataBaseInfoMsg response)
            {
                _controller?.Tell(response);
                StateData.IncidentId = response.Id;
                return GoTo(IncidentStates.ReadyToProcessing);
            }
            return null;
        }

        private State<IncidentStates, IncidentStateDatа> LogicReadyToProcessing(Event<IncidentStateDatа> fsmEvent)
        {
            if (fsmEvent.FsmEvent is StateTimeout timeout)
            {
                Console.WriteLine($"FSM c Id {StateData.IncidentId} долго не обрабатывается");
                return GoTo(IncidentStates.LongProcessWaiting);
            }
            return null;
        }

        private State<IncidentStates, IncidentStateDatа> LogicLongProcessWaiting(Event<IncidentStateDatа> fsmEvent)
        {
            throw new NotImplementedException();
        }

        private State<IncidentStates, IncidentStateDatа> LogicProcessing(Event<IncidentStateDatа> fsmEvent)
        {
            throw new NotImplementedException();
        }

        private State<IncidentStates, IncidentStateDatа> LogicProcessingFinished(Event<IncidentStateDatа> fsmEvent)
        {
            throw new NotImplementedException();
        }

        private class SaveInDataBaseRequest
        {
        }
    }
}
