﻿namespace IncidentsQueue.Database
{
    /// <summary>
    /// Репозиторий инцидентов
    /// </summary>
    public interface IIncidentsRepository
    {
        /// <summary>
        /// Добавление новой записи об инциденте в БД
        /// </summary>
        /// <param name="incident">Информация об инциденте</param>
        /// <returns>Присвоенный инциденту Id</returns>
        long AddIncident(IncidentBase incident);
    }
}
