﻿namespace IncidentsQueue
{
    public class IncidentStateDatа
    {
        public long IncidentId { get; set; }
        public IncidentBase Incident { get; set; }
        public CmaDispatcher CurrentDispatcher { get; set; }
    }
}