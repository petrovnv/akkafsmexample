﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IncidentsQueue.Messages
{
    public class IncidentFrontDataRequest
    {
        public long IncidentId { get; set; }
    }

    public abstract class IncidentFrontDataResponseBase
    {
    }

}
