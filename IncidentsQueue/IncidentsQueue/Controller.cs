﻿using System.Collections.Generic;
using Akka.Actor;
using IncidentsQueue.Database;
using IncidentsQueue.Messages;

namespace IncidentsQueue
{
    public class Controller : ReceiveActor
    {
        private readonly IIncidentsRepository _repository;

        private Dictionary<long, IActorRef> _incidentsInProcess;

        public Controller(IIncidentsRepository repository)
        {
            _repository = repository;
            Receive<IncidentBase>(t => true, OnAddIncident);
            Receive<SaveInDataBaseInfoMsg>(t => true, OnIncidentRegistered);
            Receive<IncidentFrontDataRequest>(t => true, x =>
            {
                IActorRef fsm = _incidentsInProcess[x.IncidentId];
                fsm.Ask<IncidentFrontDataResponseBase>(x).PipeTo(Sender);
            });
        }

        protected override void PreStart()
        {
            //todo: загрузка из БД необработанных инцидентов
            _incidentsInProcess = new Dictionary<long, IActorRef>();
        }

        private void OnAddIncident(IncidentBase incident)
        {
            Context.ActorOf(Props.Create(() => new IncidentStateActor(Self, _repository, incident)));
        }

        private void OnIncidentRegistered(SaveInDataBaseInfoMsg msg)
        {
            _incidentsInProcess[msg.Id] = Sender;
        }
    }
}
