﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Akka.Actor;
using IncidentsQueue;
using IncidentsQueue.Messages;
using Runner.Database;

namespace Runner
{
    class Program
    {
        static void Main(string[] args)
        {
            IncidentsRepository repository = new IncidentsRepository();


            ActorSystem system = ActorSystem.Create("CMA");
            var controller = system.ActorOf(Props.Create(() => new Controller(repository)));

            for (int i = 0; i < 10000; i++)
            {
                controller.Tell(new Accident()); //Accident - это реализация IncidentBase для ДТП
            }

            Task<IncidentFrontDataResponseBase> task =
                controller.Ask<IncidentFrontDataResponseBase>(new IncidentFrontDataRequest() {IncidentId = 5000});

            Console.Read();

        }
    }
}
