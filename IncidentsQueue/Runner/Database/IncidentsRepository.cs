﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IncidentsQueue;
using IncidentsQueue.Database;

namespace Runner.Database
{
    class IncidentsRepository : IIncidentsRepository
    {
        public long AddIncident(IncidentBase incident)
        {
            using (IncidentsDbContext dbContext = new IncidentsDbContext())
            {
                IncidentEntity incidentEntity = new IncidentEntity()
                {
                    Date = DateTime.Now
                };

                dbContext.Incidents.Add(incidentEntity);
                dbContext.SaveChanges();
                return incidentEntity.Id;
            }
        }
    }
}
