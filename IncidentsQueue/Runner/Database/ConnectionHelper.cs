﻿using System.Data.SqlClient;

namespace Runner.Database
{
    public static class ConnectionHelper
    {
        private static string _connectionStr;

        public static string ConnectionString
        {
            get
            {
                if (_connectionStr == null)
                {
                    _connectionStr = new SqlConnectionStringBuilder
                    {
                        DataSource = @".\SQLEXPRESS",
                        InitialCatalog = "Incidents",
                        IntegratedSecurity = true
                    }.ConnectionString;

                }
                return _connectionStr;
            }
        }
    }
}
