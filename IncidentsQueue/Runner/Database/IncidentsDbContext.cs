﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Runner.Database
{
    internal class IncidentsDbContext : DbContext
    {
        public IncidentsDbContext() : base(ConnectionHelper.ConnectionString)
        {
            
        }

        public DbSet<IncidentEntity> Incidents { get; set; }
    }
}
